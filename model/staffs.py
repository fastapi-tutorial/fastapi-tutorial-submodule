from pydantic import BaseModel, Field

class Staff(BaseModel):
    staff_id: str = Field(default=None)
    nick_name: str = Field(default=None)