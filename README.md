# FastAPI Tutorial

## Install project liabry
```
pip install -r requirements.txt
```

## Activate virtual environment
```
env1/Scripts/activate
```

### Runserver and hot-reloads for development
```
uvicorn main:app --reload
```
##### or
```
python -m uvicorn main:app --reload
```