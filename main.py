from fastapi import FastAPI
from routes.staffs import staffs

app = FastAPI()

app.include_router(staffs)


