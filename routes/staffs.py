from bson import ObjectId
from fastapi import APIRouter
from config.database import db
from model.staffs import Staff

staffs = APIRouter(
    prefix='/staffs',
    tags=['staff']
)

@staffs.get("/", status_code=200)
async def get_staff():
    staff_item = db.staffs.find()
    staffs= []
    for item in staff_item:
        staffs.append(item)
    return {
        'Message': 'Get staff success.',
        'Data': staffs
    }
    
@staffs.post('/')
async def add_staff(staff: Staff):
    db.staffs.insert_one(dict(staff))
    return {
        'Message': 'Add staff success.'
    }
    
@staffs.put('/{oid}')
async def update_staff(oid, staff: Staff):
    db.staffs.update_one({'_id', ObjectId(oid)},
                         {'$set': dict(staff)}
                        )
    return {
        'Message': 'Update staff success.'
    }
    
@staffs.delete('/{oid}')
async def delete_staff(oid):
    db.staff.delete_one({'_id', ObjectId(oid)})
    return {
        'Message': 'Delete staff success.'
    }
    