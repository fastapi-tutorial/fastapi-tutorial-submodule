from pymongo import MongoClient
try:
    # DB_HOST = 'localhost'
    # DB_PORT = '27017'
    # client = MongoClient(f'mongodb://{DB_HOST}{DB_PORT}/')
    client = MongoClient()
    db = client.test_database
    print('Connected to Database.')
except Exception as error:
    print(str(error))